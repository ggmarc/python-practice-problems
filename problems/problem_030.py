# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):

    if len(values) == 0 or len(values) == 1:
        return None

    f_max = values[0]
    s_max = values[0]

    for item in values:
        if item > f_max:
            s_max = f_max
            f_max = item
        elif item > s_max:
            s_max = item
    return s_max # THIS WILL PRINT / RETURN S_MAX VALUE

print(find_second_largest([1,3,2,1]))
