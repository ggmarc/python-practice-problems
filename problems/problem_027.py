# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):

    if len(values) == 0:
        return None


    max_value = values[0] #Have to set max_value to the index of 0 in values

    for item in values:    # for each item in values
        if item > max_value: # if item is greater than max_value
            max_value = item # it will then change the max_value or the largest number to the item
    return max_value # then runs again

print(max_in_list([2,4141,1,3]))
