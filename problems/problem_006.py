# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    # need to check age, input age
    # need boolean for has consent form
    # can check first if signed consent form, just terminate code

        if age >= 18 or has_consent_form:
            return True
        else:
            return False

print(can_skydive(4, False))
